const promiseThen = new Promise((resolve, reject) => {
    setTimeout(() => {
        resolve('Hi!');
    }, 3000);
});

promiseThen
    .then((val) => {
        console.log(val);
    })
    .catch((err) => console.log(err));