import { useSelector } from 'react-redux';
import TodoItem from './TodoItem';
import './TodoItem.css';
import './TodoGroup.css';


const TodoGroup = () => {

    const todoItems = useSelector((state) => state.todo.todoItems);

    return (
        <div className="todo-group-container">
            {todoItems.map((todoItem) => (
                <TodoItem key={todoItem.id} todoItem={todoItem} />
            ))}
        </div>
    )
}

export default TodoGroup;