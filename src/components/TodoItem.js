import { useDispatch } from 'react-redux';
import { deleteTodo, changeCompleteStatus } from '../app/TodoSlice';
import './TodoItem.css';


const TodoItem = ({ todoItem }) => {

    const dispatch = useDispatch();

    const handleOnClickTodoItemText = () => {
        dispatch(changeCompleteStatus(todoItem.id));
    } 

    const handleOnClickDeleteButton = (event) => {
        event.stopPropagation();
        if (window.confirm('Are you sure you want to delete this todo item?')) {
            dispatch(deleteTodo(todoItem.id));
        }
    }

    return (
        <div className='todo-item'
        onClick={handleOnClickTodoItemText}>
            <span className={todoItem.done ? "done" : ""}>
                {todoItem.text}
            </span>
            <button className="delete-button" onClick={handleOnClickDeleteButton}>
                X
            </button>
        </div>
    )
}

export default TodoItem;
