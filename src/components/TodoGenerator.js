import { useState } from 'react';
import { useDispatch } from 'react-redux';
import { addTodo } from '../app/TodoSlice';


function TodoGenerator () {
    
    const [text, setText] = useState('')
    const dispatch = useDispatch()

    const handleChange = (event) => {
        setText(event.target.value)
    }

    const handleAdd = () => {
        if (text.trim() !== "") {
            dispatch(addTodo(text));
        } else {
            alert("Todo item cannot be empty!");
        }
        
    }

    return (
        <div>
            <input type="text" value={text} onChange={handleChange} />
            <button onClick={handleAdd}>Add</button>
        </div>
    )
}

export default TodoGenerator;