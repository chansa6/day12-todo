import { createSlice } from "@reduxjs/toolkit";
import { v4 as uuidv4 } from "uuid";

const initialState = {
    todoItems: [],
};

const todoSlice = createSlice({
    name: "todo",
    initialState,
    reducers: {
        addTodo: (state, action) => {
            state.todoItems.push({
                id: uuidv4(),
                text: action.payload,
                done: false
            });
        },
        deleteTodo: (state, action) => {
            state.todoItems = state.todoItems.filter(
                (todoItem) => todoItem.id !== action.payload
            );
        },
        changeCompleteStatus: (state, action) => {
            var foundIndex = state.todoItems.findIndex(
                (arrItem) => arrItem.id === action.payload
            );
            state.todoItems[foundIndex].done =
                !state.todoItems[foundIndex].done;
        },
    }
});

export const { addTodo, deleteTodo, changeCompleteStatus } = todoSlice.actions;
export default todoSlice.reducer;